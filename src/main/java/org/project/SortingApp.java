package org.project;
import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class represents a sorting application.
 * It provides a method for sorting arrays of integers.
 */
public class SortingApp {
    private static Logger logger = LogManager.getLogger(SortingApp.class);
    /**
     * This method sorts the given array of integers in ascending order.
     * @param array the array to be sorted.
     * @throws NullPointerException if the array is null.
     */
    public void sort(int[] array) {
        if (array == null) {
            logger.error("Input array is null");
            throw new NullPointerException();
        } else if (array.length>10) {
            logger.error("Input array has more than 10 elements");
            throw new IllegalArgumentException("The input array should have max 10 elements");
        } else if (array.length==0) {
            logger.error("Input array has no elements");
            throw new IllegalArgumentException("The input array should have at least one element");
        }else{
            logger.info("Sorting array:" + Arrays.toString(array));
            Arrays.sort(array);
            logger.info("Sorted array:" + Arrays.toString(array));
        }
    }
}

