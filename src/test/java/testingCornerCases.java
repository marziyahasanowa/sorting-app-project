import org.junit.Test;
import org.project.SortingApp;
import static org.junit.Assert.*;

/**
 Test for corner cases of the SortingApp class.
 */
public class testingCornerCases {
    private SortingApp sortingApp = new SortingApp();

    /**
     * Test case for sorting a null array.
     * Expects a NullPointerException to be thrown.
     */

        @Test(expected = NullPointerException.class)
        public void testNullCase() {
            int[] array = null;
            sortingApp.sort(array);
        }

    /**
     Test case for sorting an empty array.
     Expects an IllegalArgumentException to be thrown.
     */
        @Test (expected = IllegalArgumentException.class)
        public void testEmptyCase(){
            int[] array = new int[0];
            sortingApp.sort(array);
            assertArrayEquals(new int[0], array);
        }

    /**
     Test case for sorting an array with a single element.
     Expects the array to remain unchanged.
     */
        @Test
        public void testSingleElementArrayCase() {
            int[] array = new int[1];
            sortingApp.sort(array);
            assertArrayEquals(new int[1], array);
        }

    /**
     Test case for sorting an array with ten elements.
     Expects the array to be sorted in ascending order.
     */
        @Test
        public void testTenElementsCase() {
            int[] array = {9, 5, 2, 8, 1, 7, 4, 6, 3, 0};
            sortingApp.sort(array);
            int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            assertArrayEquals(expected, array);
        }

    /**
     Test case for sorting an array with more than ten elements.
     Expects an IllegalArgumentException to be thrown.
     */
        @Test(expected = IllegalArgumentException.class)
        public void testMoreThanTenElementsCase() {
            int[] array = {15, 9, 2, 8, 12, 7, 4, 6, 10, 3, 14, 1, 11, 13, 0};
            sortingApp.sort(array);
        }


}
