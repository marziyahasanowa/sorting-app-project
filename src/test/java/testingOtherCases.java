import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.project.SortingApp;
import java.util.Arrays;
import static org.junit.Assert.*;

/**
 * Parameterized test for testing SortingApp with various array cases.
 */
@RunWith(Parameterized.class)
public class testingOtherCases {
    private SortingApp sortingApp = new SortingApp();
    private int[] array;

    /**
     * Constructs a new instance of the testingOtherCases with the given array parameters.
     *
     * @param array is the input array for the test.
     */
    public testingOtherCases (int[]array){
        this.array = array;
    }

    /**
     * Provides the input arrays for the parameterized test.
     * @return the input arrays to be used in the test.
     */
    @Parameters
    public static Object[][] elements() {
        return new Object[][]{
                {new int[]{-22, -3, -102, -32}},
                {new int[]{-2, -4, 0, 12, -44}},
                {new int[]{-99, 77, 2}},
                {new int[]{12, 13, -14, 15, -16}},
                {new int[]{5, 4, 3, 2, 1}}
        };
    }

    /**
     * Tests the sorting behavior for the input array.
     * It checks whether the array is sorted in ascending order.
     */
    @Test
    public void testOtherCases () {
        int[] expected = array.clone();
        Arrays.sort(expected);
        sortingApp.sort(array);
        assertArrayEquals(expected, array);
    }
}


