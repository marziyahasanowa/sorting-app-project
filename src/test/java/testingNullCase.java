import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.project.SortingApp;

/**
 * Parameterized test for testing SortingApp with null array cases.
 */
@RunWith(Parameterized.class)
public class testingNullCase {
    private SortingApp sortingApp = new SortingApp();
    private int[] array;
    /**
     * Constructs a new instance of the testingNullCase with the given array parameters.
     *
     * @param array is the input array for the test.
     */
    public testingNullCase(int[] array) {
        this.array = array;
    }
    /**
     * Provides the input arrays for the parameterized test.
     * @return the input arrays to be used in the test.
     * Includes both failing and passing test cases.
     */
    @Parameters
    public static Object[][] elements() {
        return new Object[][]{
                {null},
                {new int[]{1, 2, 3}} //fails, expected exception when null
        };
    }
    /**
     * Tests the sorting behavior for the input array.
     * It checks whether the sorted array null.
     */
    @Test(expected = NullPointerException.class)
    public void testNullCase() {
        sortingApp.sort(array);
    }
}
