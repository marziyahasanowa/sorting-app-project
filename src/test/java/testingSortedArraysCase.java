import org.junit.Test;
import org.junit.runners.Parameterized;
import org.project.SortingApp;
import java.util.Arrays;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

/**
 * Parameterized test for testing SortingApp with sorted array cases.
 */
@RunWith(Parameterized.class)
public class testingSortedArraysCase {
    private SortingApp sortingApp = new SortingApp();
    private int[] array;

    /**
     * Constructs a new instance of the testingSortedArrayCase with the given array parameters.
     *
     * @param array is the input array for the test.
     */
    public testingSortedArraysCase (int[]array){
        this.array = array;
    }

    /**
     * Provides the input arrays for the parameterized test.
     * @return the input arrays to be used in the test.
     */
    @Parameters
    public static Object[][] elements() {
        return new Object[][]{
                {new int[]{1,3,5,7}},
                {new int[]{4, 5, 6}},
                {new int[]{-1,0,1}},
                {new int[]{7,8,9}}
        };
    }

    /**
     * Tests the sorting behavior for the input array.
     * It checks whether the array is sorted in ascending order.
     */
    @Test
    public void testSortedArrayCase () {
        int[] expected = array.clone();
        Arrays.sort(expected);
        sortingApp.sort(array);
        assertArrayEquals(expected, array);
    }
}
