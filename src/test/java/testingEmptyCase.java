import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.project.SortingApp;
import static org.junit.Assert.*;
import org.junit.runners.Parameterized.Parameters;

/**
 * Parameterized test for testing SortingApp with empty array cases.
 */
@RunWith(Parameterized.class)
public class testingEmptyCase {
    private SortingApp sortingApp = new SortingApp();
    private int[] array;

    /**
     * Constructs a new instance of the testingEmptyCase with the given array parameters.
     *
     * @param array is the input array for the test.
     */
    public testingEmptyCase (int[]array){
        this.array = array;
    }
    /**
     * Provides the input arrays for the parameterized test.
     * @return the input arrays to be used in the test.
     * Includes both failing and passing test cases.
     */
    @Parameters
    public static Object[][] elements() {
        return new Object[][]{
                {new int[]{}},
                {new int[0]},
                {new int[]{1,2,3}}, //fails, expected 0, actual 3
                {new int[5]} //fails, expected 0, actual 5
        };
    }
    /**
     * Tests the sorting behavior for the input array.
     * It checks whether the sorted array is empty.
     */
    @Test (expected = IllegalArgumentException.class)
    public void testEmptyCase () {
        sortingApp.sort(array);
    }
}
