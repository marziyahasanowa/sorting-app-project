import org.junit.runners.Parameterized;
import org.project.SortingApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

/**
 * Parameterized test for testing SortingApp with array with single element cases.
 */
@RunWith(Parameterized.class)
public class testingSingleElementCase {
    private SortingApp sortingApp = new SortingApp();
    private int[] array;

    /**
     * Constructs a new instance of the testingSingleElementCase with the given array parameters.
     *
     * @param array is the input array for the test.
     */
    public testingSingleElementCase (int[]array){
        this.array = array;
    }

    /**
     * Provides the input arrays for the parameterized test.
     * @return the input arrays to be used in the test.
     * Includes both failing and passing test cases.
     */
    @Parameters
    public static Object[][] elements() {
        return new Object[][]{
                {new int[]{11}},
                {new int[]{43}},
                {new int[]{-88}},
                {new int[1]},
                {new int[]{0}},
                {new int[]{77, 88, 99}}, //fails, expected 1, actual 3
                {new int[10]} //fails, expected 1, actual 10
        };
    }

    /**
     * Tests the sorting behavior for the input array.
     * It checks whether the sorted array has single element.
     */
    @Test
        public void testSingleElementCase () {
        sortingApp.sort(array);
        assertEquals(1, array.length);
        }
}
